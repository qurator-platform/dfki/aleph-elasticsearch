FROM elasticsearch:7.4.2

RUN bin/elasticsearch-plugin install --batch discovery-gce
RUN bin/elasticsearch-plugin install --batch repository-s3
RUN bin/elasticsearch-plugin install --batch repository-gcs
RUN bin/elasticsearch-plugin install --batch analysis-icu

COPY synonames.txt /usr/share/elasticsearch/config/
RUN chown elasticsearch /usr/share/elasticsearch/config/synonames.txt
#Fix k8s health check for older image version of elastic
RUN mkdir -p /opt/bitnami/scripts/elasticsearch
RUN echo exit 0 > /opt/bitnami/scripts/elasticsearch/healthcheck.sh
RUN chmod +x /opt/bitnami/scripts/elasticsearch/healthcheck.sh
